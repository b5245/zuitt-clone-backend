const express = require(`express`)
const router = express.Router()
const courseController = require(`../Controller/CourseController`)
const auth = require(`../auth`)

router.get(`/all`, courseController.getCourses)

router.post(`/addcourse`, auth.verify, courseController.addCourse)

router.post(`/getCourse`, courseController.getCourse)

module.exports = router;