const express = require(`express`)
const router = express.Router()
const userController = require(`../Controller/UserController`)
const auth = require(`../auth`)

router.post(`/register`, userController.register)

router.get(`/`, userController.getUsers)

router.post(`/login`, userController.login)

router.post(`/enroll`, auth.verify, userController.enroll)

router.post(`/checkEmail`, userController.checkEmail)

router.get(`/details`, auth.verify, userController.details)

router.get('/getEnrolled', auth.verify, userController.getEnrolled)

router.get('/getTotal', auth.verify, userController.getTotal)

module.exports = router;