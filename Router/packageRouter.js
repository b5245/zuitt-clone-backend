const express = require(`express`)
const router = express.Router()
const packageController = require(`../Controller/PackageController`)
const auth = require(`../auth`)

router.get(`/`, packageController.getAll)

router.post('/addPackage', packageController.addPackage)

router.post('/getPackage', packageController.getPackage)

router.post('/getSpecific', packageController.getSpecific)


module.exports = router;