const express = require(`express`)
const app = express()
const mongoose = require(`mongoose`)
const port = 4000
const cors = require('cors')

app.use(cors());
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

const userRoute = require(`./Router/userRouter`)
app.use(`/users`, userRoute)

const courseRoute = require(`./Router/courseRouter`)
app.use(`/courses`, courseRoute)

const packageRoute = require(`./Router/packageRouter`)
app.use(`/packages`, packageRoute)





mongoose.connect(`mongodb+srv://admin:admin@wdc028-course-booking.mgfy3.mongodb.net/Zuitt-Course-Booking?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, () => console.log(`Connected to MongoDB`))

app.listen(process.env.PORT || port, () => { console.log(`API now online on port ${process.env.PORT || port}`) })