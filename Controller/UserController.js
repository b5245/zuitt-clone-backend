const User = require(`../Models/Users`)
const Course = require(`../Models/Course`)
const bcrypt = require(`bcrypt`)
const auth = require(`../auth`)
const Package = require("../Models/Package")

module.exports = {

    register: (req, res) => {
        const newUser = new User({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            mobileNumber: req.body.mobileNumber,
            email: req.body.email,
            password: bcrypt.hashSync(req.body.password, 10)
        })

        newUser.save().then((success, error) => {
            if (error) {
                res.send(false)
            }
            else {
                res.send(true)
            }
        })
    },

    getUsers: (req, res) => {
        User.find().then(result => {
            res.send(result)
        })
    },

    login: (req, res) => {

        User.findOne({ email: req.body.email }).then(result => {
            if (result == null) {
                res.send(false)
            }
            else {
                const passMatch = bcrypt.compareSync(req.body.password, result.password)
                if (passMatch == true) {
                    res.send({ access: auth.createAccessToken(result) })
                }
                else {
                    res.send(false)
                }
            }
        })

    },

    enroll: (req, res) => {
        const user = auth.decode(req.headers.authorization)

        User.findById(user.id).then(userResult => {
            Package.findById(req.body.id).then(packageResult => {
                userResult.enrollments.push({
                    packageId: req.body.id,
                    name: packageResult.name,
                    price: packageResult.price

                })
                userResult.save()
            })
        })
            .then((success, error) => {
                if (error) {
                    res.send(false)
                }
                else {
                    res.send(true)
                }
            })
    },

    checkEmail: (req, res) => {
        User.findOne({ email: req.body.email }).then(result => {
            if (result == null) {
                res.send(false)
            }
            else {
                res.send(result)
            }
        })
    },

    details: (req, res) => {
        const user = auth.decode(req.headers.authorization)
        User.findById(user.id).then(result => {
            res.send(result)
        })
    },

    getEnrolled: (req, res) => {

        const user = auth.decode(req.headers.authorization)

        User.findById(user.id).then(result => {
            res.send(result.enrollments)
        })
    },

    getTotal: (req, res) => {
        const user = auth.decode(req.headers.authorization)
        let total;
        let sum = 0
        User.findById(user.id).then(result => {
            let en = result.enrollments
            let total = []
            for (let i = 0; i < en.length; i++) {
                total.push(parseInt(en[i].price))
            }

            res.send(total)

        })

    }

}