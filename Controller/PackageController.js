const Package = require(`../Models/Package`)
const auth = require(`../auth`)

module.exports = {

    getAll: (req, res) => {
        Package.find().then(result => {
            res.send(result)
        })
    },

    addPackage: (req, res) => {
        let newPackage = new Package({
            name: req.body.name,
            description: req.body.description,
            price: req.body.price,
            courseId: req.body.courseId
        })

        newPackage.save().then((success, error) => {
            if (error) {
                res.send(false)
            }
            else {
                res.send(true)
            }
        })
    },

    getPackage: (req, res) => {
        Package.find({ courseId: req.body.courseId }).then(result => {
            res.send(result)
        })
    },

    getSpecific: (req, res) => {
        Package.findById(req.body.id).then(result => {
            res.send(result)
        })
    }



}