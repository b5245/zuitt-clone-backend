const Course = require(`../Models/Course`)
const auth = require(`../auth`)


module.exports = {

    getCourses: (req, res) => {
        Course.find().then(result => {
            res.send(result)
        })
    },

    addCourse: (req, res) => {
        const user = auth.decode(req.headers.authorization)

        if (user.isAdmin) {
            let newCourse = new Course({
                name: req.body.name,
                detail: req.body.detail,
                description: req.body.description,
                price: req.body.price,
            })

            newCourse.save().then((success, error) => {
                if (error) {
                    res.send(false)
                }
                else {
                    res.send(true)
                }
            })
        }
        else {
            res.send(false)
        }
    },

    getCourse: (req, res) => {
        Course.findById(req.body.id).then(result => {
            res.send(result)
        })
    }
}